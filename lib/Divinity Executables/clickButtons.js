module.exports = function(){

let clickButtons = this;

let mainPage = new (require("../page objects/main_page"))

clickButtons.execute = () => describe("test1 -- clicking 'apply'", () => {
    it('should click "Apply"', function() {

        browser.get("https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/#")

        mainPage.clickApply();

        //assert that the current url matches the desired one
        //Right now apply goes to the same link ¯\_(ツ)_/¯
        expect(browser.getCurrentUrl()).toEqual('https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/#')
        
    });
    it('should click "Academics" and take us to the Bachelor\'s degree page', function() {

        //browser.get("https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/#") -- uncomment once 'apply' is fixed

        mainPage.clickAcademics();

        //assert that the current url matches the desired one
        expect(browser.getCurrentUrl()).toEqual('https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/bachelors/')
        
    });
    it('should click "Admissions" and take us to the Admissions page', function() {

        browser.get("https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/#")

        mainPage.clickAdmissions();

        //assert that the current url matches the desired one
        expect(browser.getCurrentUrl()).toEqual('https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/admissions/')
        
    });
    it('should click "About" and take us to the About page', function() {

        browser.get("https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/#")

        mainPage.clickAbout();

        //assert that the current url matches the desired one
        expect(browser.getCurrentUrl()).toEqual('https://test.edu.apps.rhocp.awsos.liberty.edu/divinity/about/')
        
    });
});
}