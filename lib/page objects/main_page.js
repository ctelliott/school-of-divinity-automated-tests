module.exports = function(){

    let mainPage = this;

    
    
    
    //This tests for clicking the Apply button

    mainPage.getApplyElement = () => {
        return element(by.css('#menu-item-520 > a'))
    }

    mainPage.clickApply = () => {
        return this.getApplyElement().click();
    }
    
    
    
    //Under here is the test that the Academics page is loading correctly

    mainPage.getAcademicsElement = () => {
        return element(by.css('#menu-item-517 > a'))
    }

    mainPage.clickAcademics = () => {
        return this.getAcademicsElement().click();
    }
     
    
    
    //Under here is the test that the Admissions page is loading correctly

    mainPage.getAdmissionsElement = () => {
        return element(by.css('#menu-item-518 > a'))
    }
    
    mainPage.clickAdmissions = () => {
        return this.getAdmissionsElement().click();
    }
    
    
    
    //Under here is the test that the About page is loading correctly

    mainPage.getAboutElement = () => {
        return element(by.css('#menu-item-516 > a'))
    }

    mainPage.clickAbout = () => {
        return this.getAboutElement().click();
    }
}