let SpecReporter = require('jasmine-spec-reporter').SpecReporter

exports.config = {

    onPrepare: function () {
      browser.driver.manage().window().maximize()
      browser.ignoreSynchronization = true
  
      // For developing tests
      jasmine.getEnv().addReporter(new SpecReporter({
          spec: {
              displayStacktrace: false
          }
      }))
    },
  
    beforeAll: function() {browser.actions()
       .sendKeys(protractor.Key.chord(protractor.Key.CONTROL, protractor.Key.SHIFT ,"p"))
       .perform();
    },

    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['tests/*.js'],

    
    capabilities: {
      browserName: 'chrome'
    },

  
    jasmineNodeOpts: {
      onComplete: null,
      isVerbose: false,
      showColors: true,
      includeStackTrace: false,
      defaultTimeoutInterval: 600000,
  
      //This empty print function stops the default protractor dot reporter from running
      //If this is erased then both will run
      print: function () { }
    },
  
    allScriptsTimeout: 2000000
  };
  